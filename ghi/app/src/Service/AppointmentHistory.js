import React from 'react';


class AppointmentHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
            key: "",
        };
        this.handleKeyChange = this.handleKeyChange.bind(this);
    }

    async componentDidMount() {
        const url = "http://localhost:8080/api/appointments/";
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState({appointments: data.appointments});
            }
        } catch (e) {
            console.error(e);
        }        
    }

    handleKeyChange(event) {
        const value = event.target.value;
        this.setState({key: value})
    }

    // filterAppointments() {
    //     let ignored = "";
    //                 if (appointment.vin !== this.state.key && (this.state.key).length > 0) {
    //                     ignored = "d-none";
    //                 }
    // }

    render () {
        return (
            <div>
                <form id="search-by-vin-form">
                    <div className="input-group mb-3 mt-5">
                        <input value={this.state.key} onChange={this.handleKeyChange} 
                        type="text" className="form-control" 
                        placeholder="Search by VIN" id="key" name="key" />
                            <span className="input-group-text"><b>Search by VIN</b></span>
                    </div>
                </form>   
                <h2 className="mt-5"><b>Service Appointments History</b></h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>VIP</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => {
                            let ignored = "";
                            if (appointment.vin !== this.state.key && (this.state.key).length > 0) {
                                ignored = "d-none";
                            }
                            return (
                                <tr key={ appointment.id } className={ignored}>
                                    <td>{ appointment.vin }</td>
                                    <td>{ appointment.is_vip ? 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="green" className="bi bi-check-circle-fill" viewBox="0 0 16 16">
                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 
                                                11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                                        </svg> : "" }
                                    </td>
                                    <td>{ appointment.owner }</td>
                                    <td>{ new Date(appointment.date).toLocaleDateString() }</td>
                                    <td>{ new Date(appointment.date).toLocaleTimeString() }</td>
                                    <td>{ appointment.technician.name }</td>
                                    <td>{ appointment.reason }</td>
                                </tr>
                            )
                        })}    
                    </tbody>
                </table>    
            </div>  
        )
    }
}
export default AppointmentHistory;
    