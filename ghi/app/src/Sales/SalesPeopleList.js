import React from 'react';


class SalesPeopleList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sales_people: []
        };
    }

    async componentDidMount() {
        const url = "http://localhost:8090/api/salespeople/";
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState({sales_people: data.sales_people});
                
            }
        } catch (e) {
            console.error(e);
        }        
    }

    render () {
        return (
            <div>
                <h2 className="mt-5"><b>Sales People List</b></h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Employee Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.sales_people.map(salesperson => {
                            return (
                                <tr key={ salesperson.id }>
                                    <td>{ salesperson.name }</td>
                                    <td>{ salesperson.employee_number}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}
export default SalesPeopleList;