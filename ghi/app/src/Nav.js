import { NavLink } from 'react-router-dom';
// import "./index.css";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light">
      <div className="container-fluid">
        <NavLink className="navbar-brand main-title" to="/"><b>Auto Dealer</b></NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">


            <li className="nav-item dropdown">
              <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" id="a1">
                <b>Inventory</b>
              </p>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/manufacturers">Manufacturers List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models">Vehicle Models List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles">Automobiles List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturers/new">New Manufacturer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models/new">New Vehicle Model</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles/new">New Automobile</NavLink></li>
              </ul>
            </li>


            <li className="nav-item dropdown">
              <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" id="a3">
                <b>Sales</b>
              </p>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/saleslist">Sales Records List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/saleshistory">Sales History</NavLink></li>
                <li><NavLink className="dropdown-item" to="/record">New Sales Record</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customer">New Customer</NavLink></li>
              </ul>
            </li>


            <li className="nav-item dropdown">
              <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" id="a4">
                <b>Service</b>
              </p>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/appointments">Service Appointments List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/history">Service Appointment History</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/new">New Appointment</NavLink></li>
              </ul>
            </li>


            <li className="nav-item dropdown">
              <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" id="a2">
                <b>Employees</b>
              </p>
              <ul className="dropdown-menu">
              <li><NavLink className="dropdown-item" to="/salespeoplelist">Sales People List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians">Technicians List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salesperson">New Sales Person</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/new">New Technician</NavLink></li>
              </ul>
            </li>



            {/* <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" to="/manufacturers/new">New Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models">Vehicle Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/new">New Vehicle Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/new">New Automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salesperson">Add Sales Person</NavLink>
            </li>
            <li className="nav-item">
             <NavLink className="nav-link" to="/customer">Add Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/record">New sales record</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/saleslist">List all sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/saleshistory">Sales person history</NavLink>
            </li> 
            <li className="nav-item">
              <NavLink className="nav-link" to="/technician/new">Add Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointment/new">New a appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointments">Service appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointment/history">Service appointment history</NavLink>
            </li> */}

          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
