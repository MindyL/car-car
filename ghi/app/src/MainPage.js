import { Link } from "react-router-dom";
import { NavLink } from 'react-router-dom';
import * as React from "react";
import "./index.css";

function MainPage() {
  return (
    <div className="px-4 mt-5 text-center">
      <h1 className="display-5 fw-bold MainpageTitle">Auto Dealer</h1>
      <div className="col-lg-10 mx-auto">
        <p className="lead mb-4 MainpageTitle1 mt-3 mb-3">
          The premiere solution for automobile dealership
          management!
        </p>


        <div className="row">
          <div className="col shoes">
            <div className="card mb-3 shadow eachCard">
              <img src="https://www.wsupercars.com/thumbnails-phone/Porsche/2020-Porsche-Taycan-Turbo-004.jpg" height="480px" className="card-img-top" alt="This is a discription"/>
              <div className="card-body mainpageBotton cardTitle1 cardTitle">
                <h4 className="card-title">INVENTORY</h4>
                <NavLink className="dropdown-item choice" to="/manufacturers">Manufacturers List</NavLink>
                <NavLink className="dropdown-item choice" to="/models">Vehicle Models List</NavLink>
                <NavLink className="dropdown-item choice" to="/automobiles">Automobiles List</NavLink>
              </div>
            </div>
          </div>
      
    
          <div className="col hats">
            <div className="card mb-3 shadow eachCard">
              <img src="https://di-uploads-development.dealerinspire.com/landroversouthshore/uploads/2022/03/ab007d7ede1b24134d76f2a64c446252x.jpg" className="card-img-top mainPicture" height="480px" alt="This is a discription"/>
              <div className="card-body mainpageBotton cardTitle2 cardTitle">
                <h4 className="card-title">SALES</h4>
                <NavLink className="dropdown-item choice" to="/saleslist">Sales Records List</NavLink>
                <NavLink className="dropdown-item choice" to="/saleshistory">Sales History</NavLink>
                <NavLink className="dropdown-item choice" to="/salespeoplelist">Sales People List</NavLink>

              </div>
            </div>
          </div>
      
          <div className="col hats">
            <div className="card mb-3 shadow eachCard">
              <img src="https://wallpaperaccess.com/full/5323386.jpg" className="card-img-top mainPicture" height="480px" alt="This is a discription"/>
              <div className="card-body mainpageBotton cardTitle2 cardTitle">
                <h4 className="card-title">SERVICES</h4>
                <NavLink className="dropdown-item choice" to="/appointments">Appointments List</NavLink>
                <NavLink className="dropdown-item choice" to="/appointments/history">Service History</NavLink>
                <NavLink className="dropdown-item choice" to="/technicians">Technicians List</NavLink>
              </div>
            </div>
          </div>
        
        </div>

      </div>
    </div>
  );
}

export default MainPage;
