import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import VehicleModelsList from './Inventory/VehicleModelsList';
import VehicleModelForm from './Inventory/VehicleModelForm';
import ManufacturersList from './Inventory/ManufacturersList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import AutomobileForm from './Inventory/AutomobileForm';
import AutomobileList from './Inventory/AutomobileList';
import TechnicianForm from './Service/TechnicianForm'
import AppointmentsList from './Service/AppointmentsList';
import TechniciansList from './Service/TechniciansList';
import AppointmentForm from './Service/AppointmentForm';
import AppointmentHistory from './Service/AppointmentHistory';
import SalesPersonForm from './Sales/SalesPersonForm';
import SalesPeopleList from './Sales/SalesPeopleList';
import CustomerForm from './Sales/CustomerForm';
import SalesRecordForm from './Sales/SalesRecordForm';
import SalesList from './Sales/SalesRecordList';
import SalesPersonHistory from './Sales/SalesPersonHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="models">
            <Route index element={<VehicleModelsList />} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechniciansList />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentsList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route> 
          <Route path="manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="new" element={<ManufacturerForm />} />  
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path='new' element={<AutomobileForm />} />
          </Route>
          <Route path='salesperson' element={<SalesPersonForm />} />
          <Route path='salespeoplelist' element={<SalesPeopleList />} />
          <Route path='customer' element={<CustomerForm />} />
          <Route path='record' element={<SalesRecordForm />} />
          <Route path='saleslist' element={<SalesList />} />
          <Route path='saleshistory' element={<SalesPersonHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
