from common.json import ModelEncoder
from .models import AutomobileVO, SaleRecord, SalesPerson, Customer


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href", "id"]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number", "id"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number", "id"]


class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = ["price", "sales_person", "customer", "vin", "id"]
    encoders = {
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
        "vin": AutomobileVOEncoder()
    }