# Generated by Django 4.0.3 on 2022-08-07 20:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0002_alter_salerecord_sales_person'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salerecord',
            name='sales_person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='sales_person', to='sales_rest.salesperson'),
        ),
    ]
