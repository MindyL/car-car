# Auto Dealer

Team:

* Mindy Lin - Service microservice
* Xiaoteng Xia- Sales microservice


## Design
This Auto Dealer app is designed for automobile dealerships, to provide an easy solution for managing their inventory, auto sales department, auto service department, employees, and customers. This project includes into three widgets: Inventory, Sales and Service.
<br>

Each member of our team was responsible for one widget.
Sheldon was responsible for Sales, Mindy was responsible for Services, and Inventory was a collaborative effort.
<br>

[Project design](https://excalidraw.com/#json=cWF5fLvYZU2WDPkfWFyK9,EuxUKR4loCRLlCpsC-YEPQ)

----

- The **Inventory** widget keeps track of manufacturers, vehicle models, and automobiles in your inventory. It allows you to:
    - access to the manufacturers list, with manufacturer’s name
    - add new manufacturers to expand your manufacturers list
    - access to the vehicle models list, with information such as name, manufacturer, and picture of the vehicle model
    - add new vehicle models to expand your vehicle models list
    - access to the automobiles list, with details such as VIN, color, year, model, and manufacturer of the automobile
    - add new automobiles to your inventory
<br>

----

- The **Service** widget keeps track of service appointments for automobiles and their owners. It allows you to:
    - access to scheduled appointments list, that contain the details such as VIN, customer name, date and time of the appointment, the assigned technician's name, and the reason for the service. On that specific page, it provides you the feature to update the appointment status as either canceled or finished.
    - add new appointments and include appointment details such as VIN of the vehicle, owner’s name, the date and time of the appointment, the assigned technician, and a reason for the service appointment (like "oil change" or "routine maintenance"). Once the appointment is created, it will be added to the scheduled appointments list.
    - access to a list of service history for a specific automobile by looking up the VIN. The service history will not only include the customer name, but also include date and time, the assigned technician's name, and the service reason of each appointment.
    - add new technicians to your system, each technician will enter a name and get a unique employee number
    - this app will also indicate the customers’ VIP status by checking the VIN for you, and reflect the VIP status on your appointments list and service history. 
<br>

----

- The **Sales** widget keeps track of automobile sales that come from the inventory. It allows you to:
    - add new sales person to your system, each sales person will enter a name and an unique employee number
    - add new customers by entering the name, address, and phone number into the customer to New Customer form
    - add new sale records once the sale are made, and the dale record will be added to the sale records list
    - access to sale records list, that contains the sales person’s name and employee number, the purchaser’s name, the automobile VIN, and the price of each sale.
    - access to each sales person’s sales history, and keep track of their performance.


## Service microservice

This **Service microservice** contains 3 models, Appointment, Technician, and AutomobileVO.
AutomobileVO is data polled from Automobile model inside the Inventory microservice. 
<br>

We use the data to compare with input VIN, and indicate if the automobile was purchased from the dealership so that the  customer will receive "VIP treatment". 
<br>

Please find the picture below for more details of each model.
![Semantic description of image](/ghi/app/public/Readme-Service.png)

## Sales microservice

This **Sales microservice** contains 4 models, Customer, SalesPerson, SaleRecord, and AutomobileVO.
AutomobileVO is data polled from Automobile model inside the Inventory microservice. 
<br>

We use the data to track of the inventory and automobiles that are available for sale. If the automobile with a specific VIN is sold, the VIN/automobile should not be included in the automobile dropdown selections when creating a new sale record. 
<br>

Please find the picture below for more details of each model.
![Semantic description of image](/ghi/app/public/Readme-Sales.png)
<br>

----

Please see below for the **MainPage** preview.
<br>

![Semantic description of image](/ghi/app/public/MainPageGIF.gif)